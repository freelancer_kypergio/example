import {Component, OnInit} from '@angular/core';

declare let BrowserPrint: any;

@Component({
    selector: 'printer-example',
    templateUrl: "app/template/main.html",
})
export class AppComponent implements OnInit {
    defaultPrinter: any;
    selectedPrinter: any;
    availablePrinters: any;
    selectedCategory: any;
    textLoading: string;
    textError: string;
    defaultMode: boolean;
    showMain: boolean = true;
    showPrinterDataLoading: boolean;
    showPrintForm: boolean;
    showPrinterDetails: boolean;
    showPrinterSelect: boolean;
    showError: boolean = false;
    zpl: string;
    today:Date;
    label:Label = {
        text : "Some middle long text in 2 lines. XXXXXXXXXX",
        code : "W-115/29"
    };


    ngOnInit(): void {
        this.initSetup();
    };

    initSetup(): void {
        this.showLoading("Loading Printer Information...");
        this.defaultMode = true;
        this.selectedPrinter = null;
        this.availablePrinters = null;
        this.selectedCategory = null;
        this.defaultPrinter = null;

        let app = this;
        BrowserPrint.getDefaultDevice('printer', function (printer: any) {
                app.defaultPrinter = printer;
                if ((printer != null) && (printer.connection != undefined)) {
                    app.selectedPrinter = printer;
                    app.hideLoading();
                    app.showPrinterDetails = true;
                    app.showPrintForm = true;
                }
                BrowserPrint.getLocalDevices(function (printers: any) {
                    app.availablePrinters = printers;

                    if (!app.availablePrinters) {
                        app.showErrorMessage("No Zebra Printers could be found!");
                        app.hideLoading();
                        app.showPrintForm = false;
                        return;
                    }
                    else if (app.selectedPrinter == null) {
                        app.defaultMode = false;
                        app.changePrinter();
                        app.showPrintForm = true;
                        app.hideLoading();
                    }
                }, undefined, 'printer');
            },
            function (error_response: any) {
                app.showErrorMessage("An error occured while attempting to connect to your Zebra Printer. You may not have Zebra Browser Print installed, or it may not be running. Install Zebra Browser Print, or start the Zebra Browser Print Service, and try again.");
            });
    };

    sendData(): void {
        this.today = new Date();
        var dd = this.today.getDate();
        var mm = this.today.getMonth()+1; //January is 0!

        var yyyy = this.today.getFullYear();
        var h = this.today.getHours();
        var m = this.today.getMinutes();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var today_ = h + ":" + m + " "  + dd+'.'+mm+'.'+yyyy;
        this.zpl = `^XA^LL200
            ^FO15,50^FB630,2,0,C^A0N80,60^FD` + this.label.text + `^FS
            ^FO15,210^FB630,2,0,C^A0N165,145^FD` + this.label.code + `^FS
            ^FO15,350^FB630,2,0,C^A0N80,60^FD` + today_ + `^FS
            ^XZ`;
        this.showLoading("Printing...");
        let app = this;
        this.checkPrinterStatus(function (text: string) {
            app.selectedPrinter.send(app.zpl, AppComponent.printComplete(app), app.printerError);
           /* if (text == "Ready to Print") {

            }
            else {
                app.printerError(text);
            }*/
        });
    };

    static printComplete(app:AppComponent): void {
        app.hideLoading();
        alert("Printing complete");
    };

    checkPrinterStatus(finishedFunction: any): void {
        this.selectedPrinter.sendThenRead("~HQES",
            function (text: string) {
                let that = this;
                let statuses = [];
                let ok = false;
                let is_error = text.charAt(70);
                let media = text.charAt(88);
                let head = text.charAt(87);
                let pause = text.charAt(84);
                // check each flag that prevents printing
                if (is_error == '0') {
                    ok = true;
                    statuses.push("Ready to Print");
                }
                if (media == '1')
                    statuses.push("Paper out");
                if (media == '2')
                    statuses.push("Ribbon Out");
                if (media == '4')
                    statuses.push("Media Door Open");
                if (media == '8')
                    statuses.push("Cutter Fault");
                if (head == '1')
                    statuses.push("Printhead Overheating");
                if (head == '2')
                    statuses.push("Motor Overheating");
                if (head == '4')
                    statuses.push("Printhead Fault");
                if (head == '8')
                    statuses.push("Incorrect Printhead");
                if (pause == '1')
                    statuses.push("Printer Paused");
                if ((!ok) && (statuses.length == 0))
                    statuses.push("Error: Unknown Error");
                finishedFunction(statuses.join());
            }, this.printerError);
    };

    changePrinter(): void {
        this.defaultMode = false;
        this.selectedPrinter = null;
        this.showPrinterDetails = false;

        if (this.availablePrinters == null) {
            this.showLoading("Finding Printers...");
            this.showPrintForm = false;
            setTimeout(this.changePrinter, 200);
            return;
        }
        this.showPrinterSelect = true;
    };

    retrySetup(): void {
        this.showMain = true;
        this.showError = false;
        this.initSetup();
    };

    showLoading(text: string): void {
        this.textLoading = text;
        this.showPrinterDataLoading = true;
        this.showPrintForm = false;
        this.showPrinterDetails = false;
        this.showPrinterSelect = false;
    };

    hideLoading(): void {
        this.showPrinterDataLoading = false;
        if (this.defaultMode == true)
            this.showPrinterDetails = true;
        else
            this.showPrinterSelect = true;
        this.showPrintForm = true;
    };

    printerError(text: string): void {
        this.showErrorMessage("An error occurred while printing. Please try again." + text);
    }

    showErrorMessage(text: string): void {
        this.textError = text;
        this.showMain = false;
        this.showError = true;
    };

    name = 'Angular';
}

export class Label{
    text : string;
    code:string;
}
